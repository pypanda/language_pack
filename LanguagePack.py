#!/usr/bin/python3

language_pack = {
    "updating": {
        "en": "updating",
        "de": "aktualisiert"
    },
    "game": {
        "en": "game",
        "de": "Spiel"
    },
    "closed": {
        "en": "closed",
        "de": "geschlossen"
    }
}
